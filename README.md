# In My Pocket browser addon for Firefox

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/1b66013669b94f77a574b39c305ef23b)](https://www.codacy.com/app/pabuisson/in-my-pocket?utm_source=pabuisson@bitbucket.org&amp;utm_medium=referral&amp;utm_content=pabuisson/in-my-pocket&amp;utm_campaign=Badge_Grade)
[![Mozilla Add-on](https://img.shields.io/amo/v/in-my-pocket.svg)](https://addons.mozilla.org/firefox/addon/in-my-pocket/)
[![](https://img.shields.io/amo/users/in-my-pocket.svg)](https://addons.mozilla.org/firefox/addon/in-my-pocket/statistics/usage/?last=365)
[![](https://img.shields.io/amo/rating/in-my-pocket.svg)](https://addons.mozilla.org/firefox/addon/in-my-pocket/reviews/)


This project is aimed to all people who are missing the old Pocket addon for Firefox. It is an unofficial browser addon for the [Pocket](http://getpocket.com/) webservice, for the moment targeting only Firefox > 45. I'm not in any way affiliated to Pocket and only does this for my fellow Firefox users looking for an easy to use Pocket extension for their favorite browser.

![In My Pocket 0.11.x logo](./assets/banner.png)

If you have suggestions on how to improve this extension or encounter some bugs, do not hesitate to contact me on Twitter [@pabuisson](https://twitter.com/pabuisson). And of course, if you like this extension, do not forget to leave a review on [Mozilla addons](https://addons.mozilla.org/en-US/firefox/addon/in-my-pocket/), it will be greatly appreciated!

## Useful links

* [In My Pocket homepage](https://inmypocketaddon.com), where you'll find a FAQ, the changelog and more info about the addon,
* [In My Pocket on addons.mozilla.org](https://addons.mozilla.org/en-US/firefox/addon/in-my-pocket/)
* [In My Pocket contact form](https://pabuisson.wufoo.com/forms/zi5scw41v368kr/)
* [In My Pocket public roadmap on Trello](https://trello.com/b/MfdNQXzX/imp-public-roadmap)
